package sharedvars

type Test struct {
	Text   string
	Number int
	List   []string
}
